package tcpmsg

import (
	"errors"
	"sync"
)

// Message holds a received buffer associated with its message signature
type Message struct {
	Sig    *MessageSig
	stream *stream
	Data   []byte
}

// Size returns the complete size of the message once it's in the stream write buffer
func (m *Message) Size() int {
	return (m.Sig.signatureLen + m.Sig.SizeLen + len(m.Data))
}

var msgPool = &sync.Pool{
	New: func() interface{} {
		return &Message{}
	},
}

// MessageRelease release the message memory to the message pool
func MessageRelease(msg *Message) {
	msg.Data = nil
	msgPool.Put(msg)
}

// ErrNoMessageYet tells the system that no messages are received yet
var ErrNoMessageYet = errors.New("no new message to read yet")
