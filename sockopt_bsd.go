// +build darwin freebsd

package tcpmsg

import (
	"fmt"
	"net"
	"os"
	"syscall"
)

func setNoSigPipe(conn net.Conn) error {
	raw, err := conn.(*net.TCPConn).SyscallConn()
	if err != nil {
		conn.Close()
		return err
	}
	err = raw.Control(func(fd uintptr) {
		if err := syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_NOSIGPIPE, 1); err != nil {
			fmt.Fprintf(os.Stderr, "could not set socket option SO_NOSIGPIPE: %v\n", err)
		}
	})
	if err != nil {
		conn.Close()
		return err
	}
	return nil
}
