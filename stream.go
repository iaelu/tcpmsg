package tcpmsg

import (
	"bytes"
	"fmt"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

type streamState int

const (
	streamStateAvail streamState = iota
	streamStateInit
	streamStateBusy
	streamStateEnd
	streamStateClose
)

const (
	defaultMaxMessageSize        int           = 64 * 1024 * 1024
	defaultStreamInactiveTimeout time.Duration = 30 * time.Second
)

// StreamStateError holds a stream state error
type StreamStateError struct {
	state   streamState
	message string
}

func (e *StreamStateError) Error() string {
	errorTxt := ""
	switch e.state {
	case streamStateAvail:
		errorTxt = "stream is currently available not holding messages"
	case streamStateInit:
		errorTxt = "stream is currently in init mode"
	case streamStateClose:
		errorTxt = e.message
	default:
		errorTxt = "an error occurred that's related to the stream state"
	}
	return errorTxt
}

var (
	bytesPool  sync.Pool
	streamPool sync.Pool
)

func reset(b []byte) {
	if len(b) == 0 {
		return
	}
	b[0] = 0
	for bp := 1; bp < len(b); bp *= 2 {
		copy(b[bp:], b[:bp])
	}
}

func newBytes(size int) *[]byte {
	if v := bytesPool.Get(); v != nil {
		b := v.(*[]byte)
		reset(*b)
		return b
	}
	b := make([]byte, size)
	return &b
}

func putBytes(b *[]byte) {
	reset(*b)
	bytesPool.Put(b)
}

type stream struct {
	conn           net.Conn
	config         StreamConfig
	writeBuffer    *bytes.Buffer
	readBuffer     *bytes.Buffer
	state          streamState
	readIncomplete bool
	lastReadTime   time.Time
	queuedSizes    []int
	sentMsg        int
	sentSize       int
	recvMsg        int
	recvSize       int
}

func newStream(conn net.Conn, config StreamConfig) *stream {
	if v := streamPool.Get(); v != nil {
		s := v.(*stream)
		s.conn = conn
		s.config = config
		s.state = streamStateInit
		s.queuedSizes = nil
		s.queuedSizes = make([]int, 0)
		s.readBuffer = nil
		s.writeBuffer = nil
		s.readBuffer = &bytes.Buffer{}
		s.writeBuffer = &bytes.Buffer{}
		s.sentMsg = 0
		s.sentSize = 0
		s.recvMsg = 0
		s.recvSize = 0
		s.readIncomplete = false
		s.lastReadTime = time.Now()
		return s
	}
	return &stream{
		conn:         conn,
		config:       config,
		state:        streamStateInit,
		queuedSizes:  make([]int, 0),
		lastReadTime: time.Now(),
		readBuffer:   &bytes.Buffer{},
		writeBuffer:  &bytes.Buffer{},
	}
}

func putStream(s *stream) {
	s.conn = nil
	s.config = nil
	s.state = streamStateInit
	s.queuedSizes = nil
	s.queuedSizes = nil
	s.readBuffer = nil
	s.writeBuffer = nil
	s.sentMsg = 0
	s.sentSize = 0
	s.recvMsg = 0
	s.recvSize = 0
	s.readIncomplete = false
	s.lastReadTime = time.Time{}
	streamPool.Put(s)
}

func (s *stream) sentMessages() int {
	return s.sentMsg
}

func (s *stream) recvMessages() int {
	return s.recvMsg
}

func (s *stream) sentBytes() int {
	return s.sentSize
}

func (s *stream) recvBytes() int {
	return s.recvSize
}

func (s *stream) queueMessage(sig *MessageSig, message []byte) error {
	mlen := len(message)
	msglen := strconv.Itoa(mlen)
	if len(msglen) > sig.SizeLen {
		return fmt.Errorf("message data length > message signature sizelen")
	}
	s.queuedSizes = append(s.queuedSizes, sig.signatureLen+sig.SizeLen+mlen)
	str := strings.Repeat("0", sig.SizeLen-len(msglen)) + msglen
	s.writeBuffer.Write(sig.Signature)
	s.writeBuffer.WriteString(str)
	s.writeBuffer.Write(message)
	return nil
}

func (s *stream) send() error {
	wblen := s.writeBuffer.Len()
	if wblen == 0 {
		if s.state == streamStateEnd {
			s.state = streamStateClose
			if err := s.conn.Close(); err != nil {
				return err
			}
		}
		return nil
	}

	if s.state != streamStateBusy && s.state != streamStateEnd {
		return fmt.Errorf("stream state not 'busy' and not 'end'")
	}

	if wblen > s.config.WriteSize() {
		wblen = s.config.WriteSize()
	}
	// // this peace of code was supposed to test non blocking write, but it instead do not wait
	// // more that the WriteDeadline. It does not break anything, it's just writing smaller
	// // chunks
	// if s.config.WriteDeadline() != 0 {
	// 	// do not block any longer than ... (timeout)
	// 	if err := s.conn.SetWriteDeadline(time.Now().Add(s.config.WriteDeadline())); err != nil {
	// 		return err
	// 	}
	// }
	buf := s.writeBuffer.Bytes()
	n, err := s.conn.Write(buf[:wblen])
	if err != nil {
		if ne, ok := err.(net.Error); !ok || !(ne.Timeout() && s.config.WriteDeadline() != 0) {
			s.state = streamStateClose
			if err2 := s.conn.Close(); err2 != nil {
				return err2
			}
			return err
		}
	}

	if n > 0 {
		s.writeBuffer.Next(n)
		s.sentSize += n
		qlen := len(s.queuedSizes)
		for qlen != 0 {
			if n < s.queuedSizes[0] {
				s.queuedSizes[0] -= n
				break
			} else {
				n -= s.queuedSizes[0]
				s.queuedSizes = s.queuedSizes[1:]
				s.sentMsg++
			}
			qlen = len(s.queuedSizes)
		}
	}

	if s.state == streamStateEnd {
		s.state = streamStateClose
		if err := s.conn.Close(); err != nil {
			return err
		}
	}
	return nil
}

func (s *stream) getMessage(messageSigs MessageSigs) (*Message, error) {
	closeConn := false
	if s.state == streamStateAvail {
		return nil, &StreamStateError{s.state, ""}
	}
	if s.state == streamStateInit {
		s.state = streamStateClose
		if err := s.conn.Close(); err != nil {
			return nil, err
		}
	}

	timer := time.Now()

	rblen := s.readBuffer.Len()
	closeMessage := ""
	if s.state == streamStateClose {
		if rblen == 0 {
			closeConn = true
			closeMessage = "closing stream since buffer is empty and marked as close"
		} else if s.readIncomplete {
			closeConn = true
			closeMessage = "closing stream since buffer is incomplete and marked as close"
		}
	} else if s.config.MaxMessageSize() != 0 && s.readBuffer.Len() > s.config.MaxMessageSize() {
		closeConn = true
		closeMessage = fmt.Sprintf("closing stream since read buffer is > defaultMaxMessageSize (%d)", s.config.MaxMessageSize())
	} else if s.config.InactiveTimeout() != 0 && time.Since(s.lastReadTime) > s.config.InactiveTimeout() {
		closeConn = true
		closeMessage = fmt.Sprintf("closing stream since it's been inactive for more than %d us", s.config.InactiveTimeout())
	}

	if closeConn {
		if s.state != streamStateClose {
			if err := s.conn.Close(); err != nil {
				return nil, err
			}
		}
		if rblen != 0 {
			s.readBuffer.Reset()
		}
		s.state = streamStateAvail
		s.readIncomplete = false
		return nil, &StreamStateError{streamStateClose, closeMessage}
	}

	s.lastReadTime = timer

	standsachance := false
	mtCount := 0
	readBuf := s.readBuffer.Bytes()
	for _, mt := range messageSigs {
		mtCount++
		if rblen < (mt.signatureLen + mt.SizeLen) {
			standsachance = true
			continue
		}
		if bytes.Compare(mt.Signature, readBuf[:mt.signatureLen]) != 0 {
			standsachance = true
			continue
		}
		sizeStr := string(readBuf[mt.signatureLen : mt.signatureLen+mt.SizeLen])
		size, _ := strconv.Atoi(sizeStr)
		if rblen < (mt.signatureLen + mt.SizeLen + size) {
			standsachance = true
			continue
		}

		msg := msgPool.Get().(*Message)
		msg.stream = s
		msg.Sig = mt
		msg.Data = make([]byte, size)
		copy(msg.Data, readBuf[mt.signatureLen+mt.SizeLen:mt.signatureLen+mt.SizeLen+size])
		s.readBuffer.Next(mt.signatureLen + mt.SizeLen + size)
		s.recvMsg++

		return msg, nil
	}

	if mtCount >= len(messageSigs) {
		if standsachance {
			s.readIncomplete = true
		} else {
			s.state = streamStateClose
			if err := s.conn.Close(); err != nil {
				return nil, err
			}
			s.readBuffer.Reset()
		}
	}

	return nil, ErrNoMessageYet
}

func (s *stream) read() error {
	if s.config.ReadDeadline() != 0 {
		// do not block any longer than ... (timeout)
		if err := s.conn.SetReadDeadline(time.Now().Add(s.config.ReadDeadline())); err != nil {
			return err
		}
	}
	buf := newBytes(s.config.ReadSize())
	defer func() { putBytes(buf) }()
	n, err := s.conn.Read(*buf)
	s.lastReadTime = time.Now()
	if err != nil {
		if ne, ok := err.(net.Error); ok && ne.Timeout() && s.config.ReadDeadline() != 0 {
			if s.state != streamStateEnd {
				s.state = streamStateBusy
			}
			return nil
		}
		s.state = streamStateClose
		if err2 := s.conn.Close(); err2 != nil {
			return err2
		}
		return err
	}
	if n <= 0 {
		s.state = streamStateClose
		if err := s.conn.Close(); err != nil {
			return err
		}
		return &StreamStateError{streamStateClose, "connection return read size <= 0"}
	}
	s.recvSize += n
	s.readBuffer.Write((*buf)[:n])
	if s.state != streamStateEnd {
		s.state = streamStateBusy
	}
	return nil
}

func (s *stream) toSend() bool {
	return s.writeBuffer.Len() > 0
}

func (s *stream) isReady() bool {
	return s.state == streamStateBusy || s.state == streamStateEnd
}

func (s *stream) isClosed() bool {
	return s.state == streamStateClose
}

func (s *stream) close() {
	s.state = streamStateEnd
}
