// +build linux

package tcpmsg

import (
	"net"
	"os/signal"
	"syscall"
)

func setNoSigPipe(conn net.Conn) error {
	signal.Ignore(syscall.SIGPIPE)
	return nil
}
