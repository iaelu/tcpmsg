package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/iaelu/tcpmsg"
)

const (
	myTypeSiren int = 10
	myTypeAddr  int = 11
	myTypeDir   int = 12
	myTypeExit  int = 13
)

var bufferPool sync.Pool

func newBuffer() *bytes.Buffer {
	if v := bufferPool.Get(); v != nil {
		b := v.(*bytes.Buffer)
		b.Reset()
		return b
	}
	return &bytes.Buffer{}
}

func putBuffer(b *bytes.Buffer) {
	b.Reset()
	bufferPool.Put(b)
}

var stderr = bufio.NewWriter(os.Stderr)

func myTypeSirenHandle(sc tcpmsg.StreamConn, msg *tcpmsg.Message) {
	buf := newBuffer()
	defer func() { putBuffer(buf) }()
	buf.Write(msg.Data)
	str, _ := buf.ReadString(0)
	fmt.Printf("mon beau siren : %s\n", str)
	buf.Reset()
	buf.WriteString("--siren--")
	if err := sc.QueueMessageByKey(myTypeSiren, buf.Bytes()); err != nil {
		fmt.Fprintf(stderr, "server.QueueMessageByKey error: %+v\n", err)
		stderr.Flush()
	}
}

func myTypeAddrHandle(sc tcpmsg.StreamConn, msg *tcpmsg.Message) {
	buf := newBuffer()
	defer func() { putBuffer(buf) }()
	buf.Write(msg.Data)
	str, _ := buf.ReadString(0)
	fmt.Printf("ma belle adresse : %s\n", str)
	buf.Reset()
	buf.WriteString("--adr--")
	if err := sc.QueueMessageByKey(myTypeAddr, buf.Bytes()); err != nil {
		fmt.Fprintf(stderr, "server.QueueMessageByKey error: %+v\n", err)
		stderr.Flush()
	}
}

func myTypeDirHandle(sc tcpmsg.StreamConn, msg *tcpmsg.Message) {
	buf := newBuffer()
	defer func() { putBuffer(buf) }()
	buf.Write(msg.Data)
	str, _ := buf.ReadString(0)
	fmt.Printf("mon bio dirigeant : %s\n", str)
	buf.Reset()
	buf.WriteString("--dir--")
	if err := sc.QueueMessageByKey(myTypeDir, buf.Bytes()); err != nil {
		fmt.Fprintf(stderr, "server.QueueMessageByKey error: %+v\n", err)
		stderr.Flush()
	}
}

func myTypeExitHandle(done chan os.Signal) tcpmsg.HandlerFunc {
	return tcpmsg.HandlerFunc(func(sc tcpmsg.StreamConn, msg *tcpmsg.Message) {
		buf := newBuffer()
		defer func() { putBuffer(buf) }()
		buf.Write(msg.Data)
		str, _ := buf.ReadString(0)
		fmt.Printf("client is calling for exit(): %s\n", str)
		buf.Reset()
		buf.WriteString("--dir--")
		if err := sc.QueueMessageByKey(myTypeExit, buf.Bytes()); err != nil {
			fmt.Fprintf(stderr, "server.QueueMessageByKey error: %+v\n", err)
			stderr.Flush()
		}
		select {
		case <-done:
			//
		default:
			close(done)
		}
	})
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("usage: TestServer port\n")
		os.Exit(-1)
	}
	port, _ := strconv.Atoi(os.Args[1])
	server, err := tcpmsg.NewServer(uint16(port), 20)
	if err != nil {
		fmt.Printf("could not create server: %v", err)
		os.Exit(1)
	}

	server.AppendMessageSig("siren", myTypeSiren, 4, myTypeSirenHandle)
	server.AppendMessageSig("adr", myTypeAddr, 4, myTypeAddrHandle)
	server.AppendMessageSig("dir", myTypeDir, 4, myTypeDirHandle)
	done := make(chan os.Signal)
	signal.Notify(done, syscall.SIGINT)
	server.AppendMessageSig("exit", myTypeExit, 4, myTypeExitHandle(done))

	go func() {
		if err := server.Serve(); err != nil {
			log.Println(err)
			if err != tcpmsg.ErrServerClosed {
				os.Exit(66)
			}
		}
	}()

	<-done
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Println(err)
	}
}
