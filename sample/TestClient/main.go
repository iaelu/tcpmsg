package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/iaelu/tcpmsg"
)

const (
	myTypeSiren int = 10
	myTypeAddr  int = 11
	myTypeDir   int = 12
	myTypeExit  int = 13

	testMsgNum int = 1000
)

func main() {
	if len(os.Args) < 3 {
		fmt.Printf("usage: TestClient server port [msgnum:1000] [eventfreq:100us]\n")
		os.Exit(-1)
	}
	port, _ := strconv.Atoi(os.Args[2])
	msgNum := testMsgNum
	if len(os.Args) >= 4 {
		n, _ := strconv.Atoi(os.Args[3])
		if n != 0 {
			msgNum = n
		}
	}
	config := tcpmsg.NewDefaultClientConfig()
	freq := config.EventFreq()
	if len(os.Args) >= 5 {
		n, _ := strconv.Atoi(os.Args[4])
		if n != 0 {
			freq = time.Duration(n)
		}
	}
	config.SetEventFreq(freq)
	config.SetServerPort(uint16(port))
	config.SetServerName(os.Args[1])

	client, err := tcpmsg.NewClientWithConfig(config)
	if err != nil {
		fmt.Printf("could not create client: %v", err)
		os.Exit(1)
	}

	buf := bytes.NewBuffer([]byte{})
	str := strings.Repeat("Z", 4999)
	buf.WriteString(str)

	client.AppendMessageSig("siren", myTypeSiren, 4)
	client.AppendMessageSig("adr", myTypeAddr, 4)
	client.AppendMessageSig("dir", myTypeDir, 4)
	client.AppendMessageSig("exit", myTypeExit, 4)

	queued := 0
	stderr := bufio.NewWriter(os.Stderr)
	start := time.Now()
	lastline := time.Now()
	h, m, s := lastline.Clock()
	fmt.Fprintf(stderr, "msgNum\tSent\tRecv\tH:M:S\n")
	fmt.Fprintf(stderr, "%d\t%d\t%d\t%02d:%02d:%02d\n", queued, client.SentMessages(), client.RecvMessages(), h, m, s)
	stderr.Flush()
	for i := 0; i < msgNum; i++ {
		if err := client.QueueMessageByKey(myTypeSiren, []byte("123456789")); err != nil {
			fmt.Printf("QueueMessageByKey error: %v", err)
			os.Exit(2)
		}
		queued++
		if err := client.QueueMessageByKey(myTypeAddr, []byte("5 rue pouet")); err != nil {
			fmt.Printf("QueueMessageByKey error: %v", err)
			os.Exit(3)
		}
		queued++
		if err := client.QueueMessageByKey(myTypeDir, buf.Bytes()); err != nil {
			fmt.Printf("QueueMessageByKey error: %v", err)
			os.Exit(4)
		}
		queued++

		if err := client.SendMessages(); err != nil {
			if err == io.EOF {
				break
			}
			fmt.Printf("Error while sending messages to server: %v\n", err)
			os.Exit(5)
		}

		for {
			msg, err := client.GetMessage()
			if err != nil {
				if err == io.EOF {
					break
				}
				if err != tcpmsg.ErrNoMessageYet {
					fmt.Fprintf(stderr, "error while receiving messages: %v\n", err)
					break
				}
			}
			if msg == nil {
				break
			}
			switch msg.Sig.Key {
			case myTypeSiren:
				fmt.Printf("mon beau siren: [%s]\n", string(msg.Data))
			case myTypeAddr:
				fmt.Printf("ma belle adresse: [%s]\n", string(msg.Data))
			case myTypeDir:
				fmt.Printf("mon bio dirigeant: [%s]\n", string(msg.Data))
			case myTypeExit:
				fmt.Printf("server exit: [%s]\n", string(msg.Data))
				fmt.Fprintf(stderr, "server exited: %v\n", time.Since(start))
			}
		}
	}
	fmt.Fprintf(stderr, "%v\n", time.Since(lastline))
	h, m, s = lastline.Clock()
	fmt.Fprintf(stderr, "%d\t%d\t%d\t%02d:%02d:%02d\n", queued, client.SentMessages(), client.RecvMessages(), h, m, s)
	stderr.Flush()

	if err := client.QueueMessageByKey(myTypeExit, []byte("exit now")); err != nil {
		fmt.Printf("QueueMessageByKey error: %v", err)
		os.Exit(6)
	}
	queued++

	for client.MessagesToSend() {
		if err := client.SendMessages(); err != nil {
			if err == io.EOF {
				break
			}
			fmt.Printf("Error while sending messages to server: %v\n", err)
			os.Exit(7)
		}

		for {
			msg, err := client.GetMessage()
			if err != nil {
				if err == io.EOF {
					break
				}
				if err != tcpmsg.ErrNoMessageYet {
					fmt.Fprintf(stderr, "error while receiving messages: %v\n", err)
					break
				}
			}
			if msg == nil {
				break
			}
			switch msg.Sig.Key {
			case myTypeSiren:
				fmt.Printf("mon beau siren: [%s]\n", string(msg.Data))
			case myTypeAddr:
				fmt.Printf("ma belle adresse: [%s]\n", string(msg.Data))
			case myTypeDir:
				fmt.Printf("mon bio dirigeant: [%s]\n", string(msg.Data))
			case myTypeExit:
				fmt.Printf("server exit: [%s]\n", string(msg.Data))
				fmt.Fprintf(stderr, "server exited: %v\n", time.Since(start))
			}
		}
	}
	fmt.Fprintf(stderr, "%v\n", time.Since(lastline))
	h, m, s = lastline.Clock()
	fmt.Fprintf(stderr, "%d\t%d\t%d\t%02d:%02d:%02d\n", queued, client.SentMessages(), client.RecvMessages(), h, m, s)
	stderr.Flush()

	for client.RecvMessages() < client.SentMessages() {
		for {
			msg, err := client.GetMessage()
			if err != nil {
				if err == io.EOF {
					break
				}
				if err != tcpmsg.ErrNoMessageYet {
					fmt.Fprintf(stderr, "error while receiving messages: %v\n", err)
					break
				}
			}
			if msg == nil {
				break
			}
			switch msg.Sig.Key {
			case myTypeSiren:
				fmt.Printf("mon beau siren: [%s]\n", string(msg.Data))
			case myTypeAddr:
				fmt.Printf("ma belle adresse: [%s]\n", string(msg.Data))
			case myTypeDir:
				fmt.Printf("mon bio dirigeant: [%s]\n", string(msg.Data))
			case myTypeExit:
				fmt.Printf("server exit: [%s]\n", string(msg.Data))
				fmt.Fprintf(stderr, "server exited: %v\n", time.Since(start))
			}
		}
	}

	fmt.Fprintf(stderr, "%v\n", time.Since(lastline))
	h, m, s = time.Now().Clock()
	fmt.Fprintf(stderr, "%d\t%d\t%d\t%02d:%02d:%02d\n", queued, client.SentMessages(), client.RecvMessages(), h, m, s)
	stderr.Flush()
}
