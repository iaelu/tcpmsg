package tcpmsg

import (
	"fmt"
	"net"
	"time"
)

const (
	defaultClientDialTimeout   time.Duration = 5 * time.Second
	defaultClientEventFreq     time.Duration = 100 * time.Microsecond
	defaultClientReadDeadline  time.Duration = time.Microsecond
	defaultClientWriteDeadline time.Duration = 100 * time.Microsecond
	defaultClientReadSize      int           = 4096
	defaultClientWriteSize     int           = 4096
)

type clientConfig struct {
	config
	serverName  string
	serverPort  uint16
	dialTimeout time.Duration
}

// ClientConfig is the client related configuration variables
type ClientConfig interface {
	StreamConfig
	ServerName() string
	SetServerName(name string) string
	ServerPort() uint16
	SetServerPort(port uint16) uint16
	DialTimeout() time.Duration
	SetDialTimeout(timeout time.Duration) time.Duration
}

// ServerPort return the server port to connect to
func (cfg *clientConfig) ServerName() string {
	return cfg.serverName
}

// SetReadSize assigns to an appropriate value the buffer read size
func (cfg *clientConfig) SetServerName(name string) string {
	cfg.serverName = name
	return name
}

// ServerPort return the server port to connect to
func (cfg *clientConfig) ServerPort() uint16 {
	return cfg.serverPort
}

// SetReadSize assigns to an appropriate value the buffer read size
func (cfg *clientConfig) SetServerPort(port uint16) uint16 {
	cfg.serverPort = port
	return port
}

// DialTimeout returns the duration before the client connect times out
func (cfg *clientConfig) DialTimeout() time.Duration {
	return cfg.dialTimeout
}

// SetCreateTimeout assigns to an appropriate value a timeout
//  for the client connection to the server
func (cfg *clientConfig) SetDialTimeout(timeout time.Duration) time.Duration {
	if timeout < 0 {
		timeout = defaultClientDialTimeout
	}
	cfg.dialTimeout = timeout
	return timeout
}

// NewDefaultClientConfig returns the recommended default client configuration
func NewDefaultClientConfig() ClientConfig {
	return &clientConfig{
		config: config{
			readSize:        defaultClientReadSize,
			writeSize:       defaultClientWriteSize,
			maxMessageSize:  defaultMaxMessageSize,
			eventFreq:       defaultClientEventFreq,
			readDeadline:    defaultClientReadDeadline,
			writeDeadline:   defaultClientWriteDeadline,
			inactiveTimeout: defaultStreamInactiveTimeout,
		},
		dialTimeout: defaultClientDialTimeout,
	}
}

// Client holds the main configuration to connect to a server
type Client struct {
	sigs          MessageSigs
	config        ClientConfig
	stream        *stream
	lastEventTime time.Time
}

// NewClientWithConfig creates a new client with a client set of configuration variables
func NewClientWithConfig(config ClientConfig) (*Client, error) {
	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", config.ServerName(), config.ServerPort()), config.DialTimeout())
	if err != nil {
		return nil, err
	}
	if err := setNoSigPipe(conn); err != nil {
		return nil, err
	}

	c := &Client{
		sigs:          make(MessageSigs),
		config:        config,
		lastEventTime: time.Time{},
	}
	c.stream = newStream(conn, config.(StreamConfig))
	c.stream.state = streamStateBusy
	return c, nil
}

// NewClientTimeout creates a new client with timeout on Dial
func NewClientTimeout(server string, port uint16, timeout time.Duration) (*Client, error) {
	config := NewDefaultClientConfig()
	config.SetServerName(server)
	config.SetDialTimeout(timeout)
	config.SetServerPort(port)
	return NewClientWithConfig(config)
}

// NewClient creates a new client
func NewClient(server string, port uint16) (*Client, error) {
	config := NewDefaultClientConfig()
	config.SetServerName(server)
	config.SetServerPort(port)
	return NewClientWithConfig(config)
}

// AppendMessagesSigs appends messages signatures from requests types
func (c *Client) AppendMessagesSigs(requests []RequestType) {
	for _, req := range requests {
		c.AppendMessageSig(req.Signature, req.Key, req.SizeLen)
	}
}

// AppendMessageSig simply implements adding a message signature
func (c *Client) AppendMessageSig(signature string, key, sizeLen int) {
	c.sigs.appendMessageSig(signature, key, sizeLen)
}

// QueueMessageByKey queues a message by its message signature key
func (c *Client) QueueMessageByKey(key int, message []byte) error {
	sig, found := c.sigs[key]
	if !found {
		return fmt.Errorf("could not find message signature by key: [%d]", key)
	}
	return c.stream.queueMessage(sig, message)
}

// SentMessages returns the count of sent messages
func (c *Client) SentMessages() int {
	return c.stream.sentMessages()
}

// RecvMessages returns the count of received messages
func (c *Client) RecvMessages() int {
	return c.stream.recvMessages()
}

// SentBytes returns how many bytes have been sent
func (c *Client) SentBytes() int {
	return c.stream.sentBytes()
}

// RecvBytes returns the count of received messages
func (c *Client) RecvBytes() int {
	return c.stream.recvBytes()
}

// MessagesToSend returns true if there are messages to send, else false
func (c *Client) MessagesToSend() bool {
	return c.stream.toSend()
}

// SendMessages sends queued messages
func (c *Client) SendMessages() error {
	return c.stream.send()
}

// GetMessage returns messages when they get available
func (c *Client) GetMessage() (*Message, error) {
	msg, err := c.stream.getMessage(c.sigs)
	if err != nil {
		if err != ErrNoMessageYet {
			return nil, err
		}
	}
	if msg != nil {
		return msg, nil
	}
	if time.Now().Sub(c.lastEventTime) > c.config.EventFreq() {
		c.lastEventTime = time.Now()
		if err := c.stream.read(); err != nil {
			c.stream.state = streamStateClose
			c.stream.conn.Close()
			return nil, err
		}
		if c.stream.state != streamStateEnd {
			c.stream.state = streamStateBusy
		}
	}
	return nil, ErrNoMessageYet
}
