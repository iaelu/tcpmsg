package tcpmsg

import "time"

// StreamConfig is the stream related configuration variables
type StreamConfig interface {
	ReadSize() int
	SetReadSize(size int) int

	WriteSize() int
	SetWriteSize(size int) int

	MaxMessageSize() int
	SetMaxMessageSize(size int) int

	EventFreq() time.Duration
	SetEventFreq(freq time.Duration) time.Duration

	ReadDeadline() time.Duration
	SetReadDeadline(delta time.Duration) time.Duration

	WriteDeadline() time.Duration
	SetWriteDeadline(delta time.Duration) time.Duration

	InactiveTimeout() time.Duration
	SetInactiveTimeout(timeout time.Duration) time.Duration
}

// config stores the global configuration for client or server
type config struct {
	readSize        int
	writeSize       int
	maxMessageSize  int
	eventFreq       time.Duration
	readDeadline    time.Duration
	writeDeadline   time.Duration
	inactiveTimeout time.Duration
}

// ReadSize return the receiving buffer maximum size
func (cfg *config) ReadSize() int {
	return cfg.readSize
}

// SetReadSize assigns to an appropriate value the buffer read size
func (cfg *config) SetReadSize(size int) int {
	cfg.readSize = size
	return size
}

// WriteSize return the sending buffer maximum size
func (cfg *config) WriteSize() int {
	return cfg.writeSize
}

// SetWriteSize assigns to an appropriate value the buffer write size
func (cfg *config) SetWriteSize(size int) int {
	cfg.writeSize = size
	return size
}

// MaxMessageSize return the sending buffer maximum size
func (cfg *config) MaxMessageSize() int {
	return cfg.maxMessageSize
}

// SetMaxMessageSize assigns to an appropriate value the buffer write size
func (cfg *config) SetMaxMessageSize(size int) int {
	cfg.maxMessageSize = size
	return size
}

// EventFreq returns the duration between each connection read
func (cfg *config) EventFreq() time.Duration {
	return cfg.eventFreq
}

// SetEventFrequency assigns to an appropriate value for how frequent
//  the client should read for messages
func (cfg *config) SetEventFreq(freq time.Duration) time.Duration {
	cfg.eventFreq = freq
	return freq
}

// ReadDeadline returns the delta duration to compute the read deadline timeout
func (cfg *config) ReadDeadline() time.Duration {
	return cfg.readDeadline
}

// SetReadDeadline assigns to an appropriate value so that the client does
//  not wait for too long on read
func (cfg *config) SetReadDeadline(delta time.Duration) time.Duration {
	cfg.readDeadline = delta
	return delta
}

// ReadDeadline returns the delta duration to compute the read deadline timeout
func (cfg *config) WriteDeadline() time.Duration {
	return cfg.writeDeadline
}

// SetWriteDeadline assigns to an appropriate value so that the client does
//  not wait for too long on write
func (cfg *config) SetWriteDeadline(delta time.Duration) time.Duration {
	cfg.writeDeadline = delta
	return delta
}

// InactiveTimeout returns the duration timeout for an inactive stream
func (cfg *config) InactiveTimeout() time.Duration {
	return cfg.inactiveTimeout
}

// SetInactiveTimeout assigns to an appropriate value how long
//  the client should wait on opened connection to close the connection
//  and return an error
func (cfg *config) SetInactiveTimeout(timeout time.Duration) time.Duration {
	cfg.inactiveTimeout = timeout
	return timeout
}
