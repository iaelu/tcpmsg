package tcpmsg

import (
	"context"
	"errors"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

const (
	defaultServerDialTimeout   time.Duration = 5 * time.Second
	defaultServerEventFreq     time.Duration = 100 * time.Microsecond
	defaultServerAcceptFreq    time.Duration = time.Millisecond
	defaultServerReadDeadline  time.Duration = time.Microsecond
	defaultServerWriteDeadline time.Duration = 100 * time.Microsecond
	defaultServerReadSize      int           = 4096
	defaultServerWriteSize     int           = 4096
)

type serverConfig struct {
	config
	serverPort uint16
	maxStreams int
	acceptFreq time.Duration
}

// ServerConfig is the server related configuration variables
type ServerConfig interface {
	StreamConfig
	ServerPort() uint16
	SetServerPort(size uint16) uint16
	MaxStreams() int
	SetMaxStreams(size int) int
	AcceptFreq() time.Duration
	SetAcceptFreq(freq time.Duration) time.Duration
}

// ServerPort return the server port to connect to
func (cfg *serverConfig) ServerPort() uint16 {
	return cfg.serverPort
}

// SetReadSize assigns to an appropriate value the buffer read size
func (cfg *serverConfig) SetServerPort(port uint16) uint16 {
	cfg.serverPort = port
	return port
}

// MaxStream return the server max streams count
func (cfg *serverConfig) MaxStreams() int {
	return cfg.maxStreams
}

// SetMaxStreams assigns the server max streams count
func (cfg *serverConfig) SetMaxStreams(maxStreams int) int {
	cfg.maxStreams = maxStreams
	return maxStreams
}

// AcceptFreq return the server accept frequency
func (cfg *serverConfig) AcceptFreq() time.Duration {
	return cfg.acceptFreq
}

// Set assigns the appropriate accept frequency value
func (cfg *serverConfig) SetAcceptFreq(freq time.Duration) time.Duration {
	if freq < 0 {
		freq = defaultServerAcceptFreq
	}
	cfg.acceptFreq = freq
	return freq
}

// NewDefaultServerConfig returns the recommended default client configuration
func NewDefaultServerConfig() ServerConfig {
	return &serverConfig{
		config: config{
			readSize:        defaultServerReadSize,
			writeSize:       defaultServerWriteSize,
			maxMessageSize:  defaultMaxMessageSize,
			eventFreq:       defaultServerEventFreq,
			readDeadline:    defaultServerReadDeadline,
			writeDeadline:   defaultServerWriteDeadline,
			inactiveTimeout: defaultStreamInactiveTimeout,
		},
	}
}

// StreamConn are methods we can only call
// during message handling
type StreamConn interface {
	QueueMessageByKey(int, []byte) error
}

// HandlerFunc is an adapter to what can be handled
// for the message signature
type HandlerFunc func(StreamConn, *Message)

type connState int

const (
	stateNew connState = iota
	stateBusy
	stateIdle
	stateClosed
)

var connStates = map[connState]string{
	stateNew:    "new",
	stateBusy:   "busy",
	stateIdle:   "idle",
	stateClosed: "closed",
}

func (c connState) String() string {
	return connStates[c]
}

type acceptedConn struct {
	srv           *Server
	conn          net.Conn
	stream        *stream
	curState      struct{ atomic int32 }
	lastEventTime time.Time
}

// QueueMessageByKey queues a message by its message signature key, on the message stream
func (ac *acceptedConn) QueueMessageByKey(key int, message []byte) error {
	sig, found := ac.srv.sigs[key]
	if !found {
		return fmt.Errorf("could not find message signature by key: [%d]", key)
	}
	return ac.stream.queueMessage(sig, message)
}

func (ac *acceptedConn) setState(state connState) {
	srv := ac.srv
	switch state {
	case stateNew:
		srv.trackConn(ac, true)
	case stateClosed:
		srv.trackConn(ac, false)
	}
	atomic.StoreInt32(&ac.curState.atomic, int32(state))
}

func (ac *acceptedConn) getState() connState {
	return connState(atomic.LoadInt32(&ac.curState.atomic))
}

func (ac *acceptedConn) serve() {
	defer func() {
		ac.setState(stateClosed)
		ac.stream.state = streamStateClose
		ac.conn.Close()
		putStream(ac.stream)
	}()

	ac.stream = newStream(ac.conn, ac.srv.config)
	ac.stream.state = streamStateBusy
	ac.setState(stateBusy)

	for {
		// server_getmsg:
		// 1. stream_getmsg
		msg, err := ac.stream.getMessage(ac.srv.sigs)
		if err != nil {
			if err != ErrNoMessageYet {
				return
			}
		}
		// 2. if msg != nil { return msg }
		if msg != nil {
			if h := msg.Sig.handle; h != nil {
				// 2.a server handling
				h(ac, msg)
			}
			MessageRelease(msg)
			// 2.b send msg on stream
			if err := ac.stream.send(); err != nil {
				return
			}
			// 2.c when server_getmsg returns a message,
			// it does not try to read
			continue
		}
		// 3. stream read if no message returned,
		// let the go scheduler arrange goroutines
		// so we do not check the last time we did
		// this event
		if err := ac.stream.read(); err != nil {
			return
		}
		// 4. send messages if there are
		if ac.stream.toSend() {
			if err := ac.stream.send(); err != nil {
				return
			}
		}
	}

}

// ErrServerClosed is returned by the serving when
// it's closed
var ErrServerClosed = errors.New("server closed")

// Server holds main configuration about a server
type Server struct {
	sigs           MessageSigs
	config         ServerConfig
	ln             net.Listener
	lastAcceptTime time.Time
	mu             sync.Mutex
	done           chan struct{}
	streamCount    int
	activeStream   map[*acceptedConn]struct{}
}

// NewServerWithConfig creates a new server with user configuration
func NewServerWithConfig(config ServerConfig) (*Server, error) {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", config.ServerPort()))
	if err != nil {
		return nil, err
	}
	s := &Server{
		sigs:           make(MessageSigs),
		config:         config,
		ln:             ln,
		activeStream:   make(map[*acceptedConn]struct{}),
		lastAcceptTime: time.Now(),
	}
	return s, nil
}

// NewServer creates a new server with default values
func NewServer(port uint16, maxStreams int) (*Server, error) {
	config := NewDefaultServerConfig()
	config.SetServerPort(port)
	config.SetMaxStreams(maxStreams)
	return NewServerWithConfig(config)
}

// AppendMessageSig simply implements adding a message signature
func (s *Server) AppendMessageSig(signature string, key, sizeLen int, handler HandlerFunc) {
	s.sigs.appendMessageSigHandling(signature, key, sizeLen, handler)
}

func (s *Server) getDoneChan() <-chan struct{} {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.getDoneChanLocked()
}

func (s *Server) getDoneChanLocked() chan struct{} {
	if s.done == nil {
		s.done = make(chan struct{})
	}
	return s.done
}

func (s *Server) closeDoneChanLocked() {
	ch := s.getDoneChanLocked()
	select {
	case <-ch:
		//
	default:
		close(ch)
	}
}

func (s *Server) trackConn(ac *acceptedConn, add bool) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if add {
		s.activeStream[ac] = struct{}{}
		s.streamCount++
	} else {
		delete(s.activeStream, ac)
		s.streamCount--
	}
}

// Serve lets the serve accept connections
func (s *Server) Serve() error {
	defer s.ln.Close()
	streamCount := 0
	for {
		cn, err := s.ln.Accept()
		if err != nil {
			select {
			case <-s.getDoneChan():
				return ErrServerClosed
			default:
			}
			return err
		}
		//s.lastAcceptTime = now
		s.mu.Lock()
		streamCount = s.streamCount
		s.mu.Unlock()
		if streamCount >= s.config.MaxStreams() {
			cn.Close()
			continue
		}
		ac := &acceptedConn{
			srv:           s,
			conn:          cn,
			lastEventTime: time.Time{},
		}
		ac.setState(stateNew)
		go ac.serve()
	}
}

func (s *Server) closeIdleConns() bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	quiescent := true
	for ac := range s.activeStream {
		st := ac.getState()
		if st == stateNew {
			st = stateIdle
		}
		if st != stateIdle {
			quiescent = false
			continue
		}
		ac.conn.Close()
		delete(s.activeStream, ac)
		s.streamCount--
	}
	return quiescent
}

const shutdownPollInterval = 500 * time.Millisecond

// Shutdown close everything and wait for clients to finish
func (s *Server) Shutdown(ctx context.Context) error {
	s.mu.Lock()
	lnerr := s.ln.Close()
	s.closeDoneChanLocked()
	s.mu.Unlock()

	ticker := time.NewTicker(shutdownPollInterval)
	defer ticker.Stop()
	for {
		if s.closeIdleConns() {
			return lnerr
		}
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-ticker.C:
		}
	}
}

// StreamCount stats only
func (s *Server) StreamCount() int {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.streamCount
}
