package tcpmsg

// MessageSig holds the signature for a message
type MessageSig struct {
	Signature    []byte
	signatureLen int
	Key          int
	SizeLen      int
	handle       HandlerFunc
}

// MessageSigs holds an array of MessageSig
type MessageSigs map[int]*MessageSig

// appendMessageSig simply implements appending to an array of MessageSig
func (m MessageSigs) appendMessageSig(signature string, key, sizeLen int) {
	m[key] = &MessageSig{
		Signature:    ([]byte)(signature),
		signatureLen: len(signature),
		Key:          key,
		SizeLen:      sizeLen,
		handle:       nil,
	}
}

// appendMessageSigHandling simply implements appending to an array of MessageSig
// with a handler function
func (m MessageSigs) appendMessageSigHandling(signature string, key, sizeLen int, handle HandlerFunc) {
	m[key] = &MessageSig{
		Signature:    ([]byte)(signature),
		signatureLen: len(signature),
		Key:          key,
		SizeLen:      sizeLen,
		handle:       handle,
	}
}
