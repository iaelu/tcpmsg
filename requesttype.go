package tcpmsg

// RequestType hold request type informations
type RequestType struct {
	Key       int
	Signature string
	SizeLen   int
}
